package acer.com.gson1;

import android.Manifest;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

class V {
    private Location location;

    @Override
    public String toString() {
        return location.toString();
    }
}

class Location {
    @SerializedName("coordinates")
    private
    Coordinates coord;

    class Coordinates {
        double latitude, longitude;
    }

    private String country;
    private String country_code;
    private int id;

    @Override
    public String toString() {
        return coord.latitude + " " + coord.longitude + " "
                + country + " " + country_code + " " + id;
    }
}


class GetInfo extends AsyncTask<Void, Void, String> {
    interface AsyncResponse {
        void processFinish(String output);
    }

    public AsyncResponse delegate = null;

    public GetInfo(AsyncResponse delegate) {
        super();
        this.delegate = delegate;
        Log.d("gsonon", "delegate");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... voids) {
        String loc = "";
        try {
            // https://www.nytimes.com/interactive/2020/03/22/world/coronavirus-spread.html
            // https://developer.android.com/reference/android/util/JsonReader?hl=ru

            // IMPORTANT
            // https://github.com/ExpDev07/coronavirus-tracker-api
            // https://github.com/javieraviles/covidAPI // less
            // http://coronavirusapi.com/
            URL url = new URL("https://coronavirus-tracker-api.herokuapp.com/v2/locations/37");
            InputStream in = (InputStream) url.getContent();
            Scanner scanner = new Scanner(in);
            loc = scanner.nextLine();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("gsonon", loc);
        return loc;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        delegate.processFinish(s);
        //super.onPostExecute(s);
    }
}

//class Listener implements View.OnClickListener {}

public class MainActivity extends AppCompatActivity {//implements GetInfo.AsyncResponse {

    String locations = " ";
    static String[] permission_storage_array = {Manifest.permission.INTERNET};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Gson gson = new Gson();

        ActivityCompat.requestPermissions(this, permission_storage_array, 1);
        setLocation(" {\"location\":{\"coordinates\":{\"latitude\":\"-12.4634\",\"longitude\":\"130.8456\"},\"country\":\"Australia\",\"country_code\":\"AU\",\"id\":10,\"last_updated\":\"2020-03-24T12:26:21.166442Z\",\"latest\":{\"confirmed\":5,\"deaths\":0,\"recovered\":0},\"province\":\"Northern Territory\",\"timelines\":{\"confirmed\":{\"latest\":5,\"timeline\":{\"2020-01-22T00:00:00Z\":0,\"2020-01-23T00:00:00Z\":0,\"2020-01-24T00:00:00Z\":0,\"2020-01-25T00:00:00Z\":0,\"2020-01-26T00:00:00Z\":0,\"2020-01-27T00:00:00Z\":0,\"2020-01-28T00:00:00Z\":0,\"2020-01-29T00:00:00Z\":0,\"2020-01-30T00:00:00Z\":0,\"2020-01-31T00:00:00Z\":0,\"2020-02-01T00:00:00Z\":0,\"2020-02-02T00:00:00Z\":0,\"2020-02-03T00:00:00Z\":0,\"2020-02-04T00:00:00Z\":0,\"2020-02-05T00:00:00Z\":0,\"2020-02-06T00:00:00Z\":0,\"2020-02-07T00:00:00Z\":0,\"2020-02-08T00:00:00Z\":0,\"2020-02-09T00:00:00Z\":0,\"2020-02-10T00:00:00Z\":0,\"2020-02-11T00:00:00Z\":0,\"2020-02-12T00:00:00Z\":0,\"2020-02-13T00:00:00Z\":0,\"2020-02-14T00:00:00Z\":0,\"2020-02-15T00:00:00Z\":0,\"2020-02-16T00:00:00Z\":0,\"2020-02-17T00:00:00Z\":0,\"2020-02-18T00:00:00Z\":0,\"2020-02-19T00:00:00Z\":0,\"2020-02-20T00:00:00Z\":0,\"2020-02-21T00:00:00Z\":0,\"2020-02-22T00:00:00Z\":0,\"2020-02-23T00:00:00Z\":0,\"2020-02-24T00:00:00Z\":0,\"2020-02-25T00:00:00Z\":0,\"2020-02-26T00:00:00Z\":0,\"2020-02-27T00:00:00Z\":0,\"2020-02-28T00:00:00Z\":0,\"2020-02-29T00:00:00Z\":0,\"2020-03-01T00:00:00Z\":0,\"2020-03-02T00:00:00Z\":0,\"2020-03-03T00:00:00Z\":0,\"2020-03-04T00:00:00Z\":1,\"2020-03-05T00:00:00Z\":1,\"2020-03-06T00:00:00Z\":0,\"2020-03-07T00:00:00Z\":0,\"2020-03-08T00:00:00Z\":0,\"2020-03-09T00:00:00Z\":0,\"2020-03-10T00:00:00Z\":1,\"2020-03-11T00:00:00Z\":1,\"2020-03-12T00:00:00Z\":1,\"2020-03-13T00:00:00Z\":1,\"2020-03-14T00:00:00Z\":1,\"2020-03-15T00:00:00Z\":1,\"2020-03-16T00:00:00Z\":1,\"2020-03-17T00:00:00Z\":1,\"2020-03-18T00:00:00Z\":1,\"2020-03-19T00:00:00Z\":1,\"2020-03-20T00:00:00Z\":3,\"2020-03-21T00:00:00Z\":3,\"2020-03-22T00:00:00Z\":5,\"2020-03-23T00:00:00Z\":5}},\"deaths\":{\"latest\":0,\"timeline\":{\"2020-01-22T00:00:00Z\":0,\"2020-01-23T00:00:00Z\":0,\"2020-01-24T00:00:00Z\":0,\"2020-01-25T00:00:00Z\":0,\"2020-01-26T00:00:00Z\":0,\"2020-01-27T00:00:00Z\":0,\"2020-01-28T00:00:00Z\":0,\"2020-01-29T00:00:00Z\":0,\"2020-01-30T00:00:00Z\":0,\"2020-01-31T00:00:00Z\":0,\"2020-02-01T00:00:00Z\":0,\"2020-02-02T00:00:00Z\":0,\"2020-02-03T00:00:00Z\":0,\"2020-02-04T00:00:00Z\":0,\"2020-02-05T00:00:00Z\":0,\"2020-02-06T00:00:00Z\":0,\"2020-02-07T00:00:00Z\":0,\"2020-02-08T00:00:00Z\":0,\"2020-02-09T00:00:00Z\":0,\"2020-02-10T00:00:00Z\":0,\"2020-02-11T00:00:00Z\":0,\"2020-02-12T00:00:00Z\":0,\"2020-02-13T00:00:00Z\":0,\"2020-02-14T00:00:00Z\":0,\"2020-02-15T00:00:00Z\":0,\"2020-02-16T00:00:00Z\":0,\"2020-02-17T00:00:00Z\":0,\"2020-02-18T00:00:00Z\":0,\"2020-02-19T00:00:00Z\":0,\"2020-02-20T00:00:00Z\":0,\"2020-02-21T00:00:00Z\":0,\"2020-02-22T00:00:00Z\":0,\"2020-02-23T00:00:00Z\":0,\"2020-02-24T00:00:00Z\":0,\"2020-02-25T00:00:00Z\":0,\"2020-02-26T00:00:00Z\":0,\"2020-02-27T00:00:00Z\":0,\"2020-02-28T00:00:00Z\":0,\"2020-02-29T00:00:00Z\":0,\"2020-03-01T00:00:00Z\":0,\"2020-03-02T00:00:00Z\":0,\"2020-03-03T00:00:00Z\":0,\"2020-03-04T00:00:00Z\":0,\"2020-03-05T00:00:00Z\":0,\"2020-03-06T00:00:00Z\":0,\"2020-03-07T00:00:00Z\":0,\"2020-03-08T00:00:00Z\":0,\"2020-03-09T00:00:00Z\":0,\"2020-03-10T00:00:00Z\":0,\"2020-03-11T00:00:00Z\":0,\"2020-03-12T00:00:00Z\":0,\"2020-03-13T00:00:00Z\":0,\"2020-03-14T00:00:00Z\":0,\"2020-03-15T00:00:00Z\":0,\"2020-03-16T00:00:00Z\":0,\"2020-03-17T00:00:00Z\":0,\"2020-03-18T00:00:00Z\":0,\"2020-03-19T00:00:00Z\":0,\"2020-03-20T00:00:00Z\":0,\"2020-03-21T00:00:00Z\":0,\"2020-03-22T00:00:00Z\":0,\"2020-03-23T00:00:00Z\":0}},\"recovered\":{\"latest\":0,\"timeline\":{}}}}}");
        GetInfo getInfo = new GetInfo(new GetInfo.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                Log.d("gsonona", output);
                setLocation(output);
            }
        });
        getInfo.execute(); // only once

        Log.d("gsonons", locations);
        V location = gson.fromJson(locations, V.class);
        Log.d("gsononc", location.toString());
        /*try {
            String res = getInfo.execute().get(2, TimeUnit.SECONDS);
            Log.d("gsonont", res);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }*/
        /*try {
            Log.d("gsononi", getInfo1.execute().get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }*/

    }

    public void setLocation(String location) {
        this.locations = location;
        Log.d("gsononsl", this.locations);

        Gson gson = new Gson();
        V location1 = gson.fromJson(locations, V.class);
        if (location1 == null) {
            Log.d("gsononc+", "null");
        } else {
            Log.d("gsononc+", location1.toString());
        }
        // local object location1
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("gsononr", locations);
    }
}
